<section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Input Data Barang</h3>
        <!-- BASIC FORM ELELEMNTS -->
        <div class="row mt">
          <div class="col-lg-10">
            <div class="form-panel">
             
              <form class="form-horizontal style-form" method="get">
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Nama Peminjam</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Kode Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Nama Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Tanggal Peminjaman</label>
                  <div class="col-sm-10">
                    <input class="form-control form-control-inline input-medium default-date-picker" size="16" type="date" value="">
                    <span class="help-block">Pilih tanggal</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Tanggal Peminjaman</label>
                  <div class="col-sm-10">
                    <input class="form-control form-control-inline input-medium default-date-picker" size="16" type="date" value="">
                    <span class="help-block">Pilih tanggal</span>
                  </div>
                </div>             

                 <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Keperluan</label>
                  <div class="col-sm-10">
                    <textarea class="form-control"> </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-10">
                   <select class="form-control">
                  <option>Pinjam</option>
                  <option>Kembali</option>
                  
                </select>
                  </div>
                </div>
                 <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <button class="btn btn-theme" type="submit">Simpan</button>
                      <button class="btn btn-theme04" type="button">Batal</button>
                    </div>
                  </div>

              </form>
            </div>
          </div>
          <!-- col-lg-12-->
        </div>

           <!--/ Konten-->

          </div>   
          </div>
          </section>
          </section>

<script src="lib/jquery-ui-1.9.2.custom.min.js"></script>

          <script type="text/javascript" src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="lib/advanced-form-components.js"></script>
