<?php 
include "config/koneksi.php";

?>


 <section id="main-content">
   <section class="wrapper">
  <h3><i class="fa fa-angle-right"></i> Tabel Data User</h3>
   
            <div class="form-panel">
             <div class="col-md-10">
              
              </div>

              <div class="col-md-2">
                  
                  <button type="button" onclick="window.location.href='?page=inputuser'" class="btn btn-success"><i class="fa fa-plus-square-o"></i> Tambah User Baru</button>

                </div><br>


        <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <table class="table table-striped table-advance table-hover">
                <h4><i class="fa fa-files-o"></i> </h4>
                <hr>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode User</th>
                    <th>Username</th>
                    <th>Nama</th>
                    <th>Level</th>
                    <th>Foto</th>                    
                    <th><i class=" fa fa-edit"></i></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                        $query =mysql_query("SELECT * FROM tbl_user ORDER BY id_user");
                        $no=1;
                        while ($var=mysql_fetch_array($query)) {
                ?> 
                  <tr>
                     <td><?php echo $no; ?></td>
                     <td><?php echo $var['id_user']; ?></td>
                     <td><?php echo $var['username']; ?></td>
                     <td><?php echo $var['nama']; ?></td>
                     <td><?php echo $var['level']; ?></td>
                     <td><img src="image/<?php echo $var['foto']; ?>" width="100" height="100"></td>
                   
                    <td>
                      
                      <a href= "?page=ubahuser&&id_user=<?php echo $var['id_user']; ?>"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                      <a href= "?page=hapususer&&id_user=<?php echo $var['id_user']; ?>"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                    </td>
                  </tr>
                 <?php
                  $no++;
                  }
                  ?>
                </tbody>
              </table>

              </section>
            </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-lg-4 -->
        </div>
         </div>
        </section>
      </section>
