<?php 
session_start();
include "config/koneksi.php";


if(!isset($_SESSION['username'])){
    header("location:/SIinve/login/");
    }
    else{
        
?>



<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Sistem Informasi Inventaris</title>

  <!-- Favicons -->
  <link href="image/icons/ums.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <script src="lib/chart-master/Chart.js"></script>

 
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="/SIinve/admin/" class="logo"><b>Sistem Informasi<span> Inventaris</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
       
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="http://localhost/SIinve/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="image/<?php echo $_SESSION['foto']; ?>" class="img-circle" height="80" width="80"></p>
          <h5 class="centered"><?php echo $_SESSION['nama'];?></h5>
          <li class="mt">
            <a class="active" href="/SIinve/admin/">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-desktop"></i>
              <span>Data Barang</span>
              </a>
            <ul class="sub">
              <li><a href="?page=inputbarang">Input Data</a></li>
              <li><a href="?page=databarang">Data</a></li>
              
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-archive"></i>
              <span>Kategori Barang</span>
              </a>
            <ul class="sub">
              <li><a href="?page=inputkategori">Input Data</a></li>
              <li><a href="?page=datakategori">Data</a></li>
              
            </ul>
          </li>
         
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-book"></i>
              <span>Peminjaman</span>
              </a>
            <ul class="sub">
              <li><a href="?page=inputpeminjam">Dosen & Karyawan</a></li>
              <li><a href="?page=inputpeminjam">Mahasiswa</a></li>
              <li><a href="?page=inputpeminjam">Ormawa</a></li>
              <li><a href="?page=datapeminjam">Data</a></li>
             
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-book"></i>
              <span>Pengambalian</span>
              </a>
            <ul class="sub">
              <li><a href="?page=pengembalian">Dosen & Karyawan</a></li>
              <li><a href="?page=inputpeminjam">Mahasiswa</a></li>
              <li><a href="?page=inputpeminjam">Ormawa</a></li>
              <li><a href="?page=datapeminjam">Data</a></li>
             
            </ul>
          </li>
                          
          <li class="sub-menu">
            <a href="javascript:;">
              <i class=" fa fa-bar-chart-o"></i>
              <span>Laporan</span>
              </a>
            <ul class="sub">
              <li><a href="?page=laporanbarang">Laporan Data Barang</a></li>
              <li><a href="?page=laporanpeminjam">Laporan Peminjaman</a></li>
              <li><a href="?page=laporanpemngembalian">Laporan Pengembalian</a></li>
              
            </ul>
          </li>
           <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-book"></i>
              <span>User</span>
              </a>
            <ul class="sub">
              <li><a href="?page=inputuser">Input Data</a></li>
              <li><a href="?page=datauser">Data</a></li>
             
            </ul>
          </li>
         
         
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    
        
          
          
             <?php    
                        if(!isset($_GET['page'])){
                            include "page/home.php";
                            }else{ 

                            $page = $_GET['page'];                     
                          switch ($page){
                            case 'inputbarang':
                            include "page/inputbarang.php";
                            break;
                            case 'databarang':
                            include "page/databarang.php";
                            break;
                            case 'laporanbarang':
                            include "page/laporanbarang.php";
                            break;
                            case 'simpanbarang':
                            include "page/simpanbarang.php";
                            break;
                            case 'simpaneditbarang':
                            include "page/simpaneditbarang.php";
                            break;
                            case 'hapusbarang':
                            include "page/hapusbarang.php";
                            break;
                            case 'ubahbarang':
                            include "page/ubahbarang.php";
                            break;
                            case 'inputkategori':
                            include "page/inputkategori.php";
                            break;
                            case 'simpankategori':
                            include "page/simpankategori.php";
                            break;
                            case 'simpaneditkategori':
                            include "page/simpaneditkategori.php";
                            break;
                            case 'hapuskategori':
                            include "page/hapuskategori.php";
                            break;
                            case 'ubahkategori':
                            include "page/ubahkategori.php";
                            break;
                            case 'datakategori':
                            include "page/datakategori.php";
                            break;
                            case 'inputpeminjam':
                            include "page/inputpeminjam.php";
                            break;
                            case 'datapeminjam':
                            include "page/datapeminjam.php";
                            break;
                             case 'laporanpeminjam':
                            include "page/laporanpeminjam.php";
                            break;
                            case 'pengembalian':
                            include "page/pengembalian.php";
                            break;
                            case 'laporanpengembalian':
                            include "page/laporanpengembalian.php";
                            break;
                            case 'inputuser':
                            include "page/inputuser.php";
                            break;
                            case 'datauser':
                            include "page/datauser.php";
                            break;
                            case 'simpanuser':
                            include "page/simpanuser.php";
                            break;
                            case 'simpanedituser':
                            include "page/simpanedituser.php";
                            break;
                            case 'hapususer':
                            include "page/hapususer.php";
                            break;
                            case 'ubahuser':
                            include "page/ubahuser.php";
                            break;
                         }
                       }

                         ?>
           
      
    <!--main content end-->
    <!--footer start-->

    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong> | Laburatorium Progdi Ilmu Komunikasi</strong>. 
        </p>
        <div class="credits">
         
          Universitas Muhammadiyah Surakarta
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
   <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>



  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
      sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
      sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '';
      nCloneTd.className = "center";

      $('#hidden-table-info thead').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
</body>

</html>
<?php  } ?>
