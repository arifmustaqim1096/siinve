 <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Form Components</h3>
        <!-- BASIC FORM ELELEMNTS -->
        <div class="row mt">
          <div class="col-lg-10">
            <div class="form-panel">

               <form class="form-horizontal style-form" method="get">
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Kode</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Nama Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Kategori Barang</label>
                  <div class="col-sm-10">
                   <select class="form-control">
                   
                   <option value="nm_kategori"> 
                      
                   </option>
                 
                </select>
                  </div>
                </div>
              

                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Merk</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Satuan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>
                 <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-10">
                    <textarea class="form-control"> </textarea>
                  </div>
                </div>
                 <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <button type="submit" class="btn btn-success" >Simpan</button>
                      <button type="reset" class="btn btn-theme04" >Batal</button>
                    </div>
                  </div>

              </form>
              
            </div>
          </div>
          <!-- col-lg-12-->
        </div>
        <!-- /row -->




           <!--/ Konten-->

        
          </section>
          </section>