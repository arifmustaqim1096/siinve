<section id="main-content">
      <section class="wrapper">



        <div class="row">
          <div class="col-lg-9 main-chart"> 
  <!--Konten-->
<div class="row mt">
          <div class="col-lg-12">
            <div class="form-panel">
              <h4 class="mb"><i class="fa fa-angle-right"></i> Pengembalian Barang</h4>
              <form class="form-horizontal style-form" method="get">
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Nama Peminjam</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Kode Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Nama Barang</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Tanggal Peminjaman</label>
                  <div class="col-sm-10">
                    <input class="form-control form-control-inline input-medium default-date-picker" size="16" type="date" value="">
                    <span class="help-block">Pilih tanggal</span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Tanggal Peminjaman</label>
                  <div class="col-sm-10">
                    <input class="form-control form-control-inline input-medium default-date-picker" size="16" type="date" value="">
                    <span class="help-block">Pilih tanggal</span>
                  </div>
                </div>             

                 <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Keperluan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-10">
                   <select class="form-control">
                  <option>Pinjam</option>
                  <option>Kembali</option>
                  
                </select>
                  </div>
                </div>
                 <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <button class="btn btn-theme" type="submit">Simpan</button>
                      <button class="btn btn-theme04" type="button">Batal</button>
                    </div>
                  </div>

              </form>
            </div>
          </div>
          <!-- col-lg-12-->
        </div>


           <!--/ Konten-->

          </div>   
          </div>
          </section>
          </section>  